package com.peddlecloud.taxcollectionprogram;

import java.util.Queue;
import java.util.Random;

public class Employee implements Employees {
    private Queue<TaxPayee> collection;
    private Queue<TaxPayee> evaluation;
    private String name;
    private int randomTime = new Random().nextInt(2);//random number between 0~2
//    private int serviceTime = (randomTime + 2) * 60;//timer around 2~4 minutes
    private int serviceTime = 5;

    public Employee(Queue<TaxPayee> collection, Queue<TaxPayee> evaluation, String name) {
        this.collection = collection;
        this.evaluation = evaluation;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void execute()
    {
        if (collection.peek() != null) {
            try {
                Thread.sleep(1000 * serviceTime);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(getName());
            collectTax(collection.poll());
        } else {
            System.out.println("No more tax payer is present.");
        }
    }

    public void collectTax(TaxPayee payee) {
        if (validateClient(payee))
        {
            System.out.println("Tax is paid for client name: " + payee.getName());
            payee.updateTaxStatus(true);
        }
    }

    public boolean validateClient(TaxPayee payee) {
        if (payee.haveData())
        {
            System.out.println(payee.getName() + "'s data is present.");
            return payee.checkEligibility();
        }
        else
        {
            System.out.println("Client's data is not present. Sending to managers.");
            evaluation.add(payee);
            return false;
        }
    }
}
