package com.peddlecloud.taxcollectionprogram;

public class MainApp {
    public static void main(String[] args) {
        int maxQueueSize = 1000;
        int employees = 5;
        int managers = 3;
        FairBooth fairBooth = new FairBooth(employees, managers, maxQueueSize);

        fairBooth.instantiate();
        fairBooth.startBooth();
    }
}
