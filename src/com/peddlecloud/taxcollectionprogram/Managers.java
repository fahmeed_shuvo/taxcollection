package com.peddlecloud.taxcollectionprogram;

public interface Managers {
    public TaxPayee newTaxPayer();
    public void execute();
}
