package com.peddlecloud.taxcollectionprogram;

public class TaxPayer implements TaxPayee {
    private String name;
    private int age;
    private long assetValue;
    private String assetType = "salary";
    private boolean taxStatus = false;
    private boolean isSet = false;

    public TaxPayer() { }

    public TaxPayer(String name, int age, long assetValue) {
        this.name = name;
        this.age = age;
        this.assetValue = assetValue;
        isSet = true;
        if (age < 20 || age > 50)
            this.assetType = "property";
    }

    public String getName() {
        return name;
    }

    public boolean haveData() {
        return isSet;
    }

    public boolean checkEligibility() {
        boolean eligible = false;
        if (age < 20 && assetValue >= 10000000 ||
                age > 50 && assetValue >= 30000000 ||
                age >= 20 && age <= 50 && assetValue >= 3000000)
        {
            eligible = true;
        }
        System.out.println("The client: " + name + " is " + (eligible ? "" : "not ") + "eligible.");
        return eligible;
    }

    public void updateTaxStatus(boolean taxStatus) {
        this.taxStatus = taxStatus;
    }
}