package com.peddlecloud.taxcollectionprogram;

import java.util.*;
import java.util.concurrent.LinkedBlockingDeque;

public class FairBooth {
    private int numberOfEmployees;
    private int numberOfManagers;
    private int maxQueueSize;
    private Managers[] manager;
    private Employees[] employee;
    private Queue<TaxPayee> collection;
    private Queue<TaxPayee> evaluation;

    public FairBooth(int employees, int managers, int maxSize) {
        numberOfEmployees = employees;
        numberOfManagers = managers;
        maxQueueSize = maxSize;
        manager = new Managers[numberOfManagers];
        employee = new Employees[numberOfEmployees];
        collection = new LinkedBlockingDeque<>(maxQueueSize);
        evaluation = new LinkedBlockingDeque<>(maxQueueSize);
    }

    public void instantiate()
    {
        for (int i = 0; i < maxQueueSize; i++) {//queue instantiation
            collection.add(new TaxPayer(randomText(), new Random().nextInt(90),
                    new Random().nextLong()));
            if (new Random().nextFloat() < .05)
            {
                collection.add(new TaxPayer());
                i++;
            }
        }

        for (int i = 0; i < numberOfManagers; i++) {//manager instantiation
            manager[i] = new Manager(collection, evaluation, ("Manager #:" + (i+1)));
        }

        for (int i = 0; i < numberOfEmployees; i++) {//employee instantiation
            employee[i] = new Employee(collection, evaluation, ("Employee #:" + (i+1)));
        }
    }

    private String randomText()
    {
        int leftLimit = 97;
        int rightLimit = 123;
        int targetStringLength = 10;
        Random random = new Random();

        return random.ints(leftLimit, rightLimit)
                .limit(targetStringLength)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();
    }

    public void startBooth()
    {
        for (Managers m : manager) {
            new Thread((Runnable) m).start();
        }
        for (Employees e : employee) {
            new Thread((Runnable) e).start();
        }
    }
}
