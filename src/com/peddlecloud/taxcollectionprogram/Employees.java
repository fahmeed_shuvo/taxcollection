package com.peddlecloud.taxcollectionprogram;

public interface Employees {
    public void execute();
    public boolean validateClient(TaxPayee payee);
    public void collectTax(TaxPayee payee);
}
