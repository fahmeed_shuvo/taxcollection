package com.peddlecloud.taxcollectionprogram;

import java.util.Queue;
import java.util.Random;
import java.util.Scanner;

public class Manager implements Managers{
    private int randomTime = new Random().nextInt(30);//random number between 0~30
//    private int serviceTime = (randomTime + 30) * 60;//timer around 30~60 minutes
    private int serviceTime = 20;
    private String name;
    private Queue<TaxPayee> collection;
    private Queue<TaxPayee> evaluation;

    public Manager(Queue<TaxPayee> collection, Queue<TaxPayee> evaluation, String name) {
        this.collection = collection;
        this.evaluation = evaluation;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void execute()
    {
        if (evaluation.peek() != null) {
            try {
                Thread.sleep(1000 * serviceTime);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            evaluation.poll();
            collection.add(newTaxPayer());
            System.out.println("Added client to the collection from evaluation by " + getName());
        } else {
            System.out.println("No more people waiting for the managers.");
        }
    }

    public TaxPayee newTaxPayer()
    {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter tax payee's name: ");
        String name = sc.next();
        System.out.println("Enter tax payee's age: ");
        int age = sc.nextInt();
        System.out.println("Enter tax payee's property/salary value: ");
        long assetValue = sc.nextLong();

        return new TaxPayer(name,age,assetValue);
    }
}
