package com.peddlecloud.taxcollectionprogram;

public interface TaxPayee {
    public boolean checkEligibility();
    public void updateTaxStatus(boolean taxStatus);
    public String getName();
    public boolean haveData();
}
